# client.py
# consume rest api
# run rest api first api_restful.py / api_pure_flask.py

import requests
import prettyprinter

base_url = "http://127.0.0.1:5000/"

def getHeadlines(source):
    '''
        get news headlines from `source` given
    '''
    url = base_url + 'headings/'
    resp = requests.get(url + source)

    return resp.json()

def getSources():
    '''
        get all news sources
    '''

    resp = requests.get(base_url + 'sources')

    return resp.json()

def getNews(source):
    '''
        get news sources
        can limit news numbers
        can check if given source has any news at all
        can fetch summary only or a certain attribute only e.g 'link' or 'images' only etc
    '''
    url = base_url + 'news/'
    resp = requests.get(url + source)

    data = resp.json()

    return data

# run functions
#source = input("Enter news source: ")

response = getNews('herald')
prettyprinter.pprint(response)
