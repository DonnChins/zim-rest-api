# api.py
# author: Donald C
# created: 20 Nov 2019
# updated: ----------
#

import flask
from flask import Flask, jsonify
from sqlalchemy import create_engine

import config

db = create_engine(f'sqlite:///{config.db_handler()}')

app = Flask(__name__)

@app.route('/')
def index():
    text = {
        'status': 'Flask Zim news rest api demo',
        'author': 'Donald C',
        'link': 'https://github.com/DonnC',
        'routes': {
            'news-sources'  : 'http://127.0.0.1:5000/sources',
            'news-headings' : 'http://127.0.0.1:5000/headings/<source-name>',
            'news-data'     : 'http://127.0.0.1:5000/news/<source-name>'
        }
    }

    return jsonify(text)

@app.route('/ping')
def ping():
   mesg = 'pong'
   return mesg

@app.route('/sources')
def sources():
   # return all news source names as they are in db as tables

    sources = config.db_tables()
    return jsonify({
            'info': 'use the source name to get info from other routes!',
            'news-sources': sources
            })

@app.route('/headings/<string:source>')
def headings(source):
    # return news headings of the particular 'source'
    conn = db.connect()

    # Perform query and return JSON data
    try:
        query = conn.execute(f"SELECT DISTINCT heading FROM {source}")

        result = {
            'status': 'OK',
            'error-msg': None,
            'headings': [i[0] for i in query.cursor.fetchall()]
            }

    except Exception:
        result = {
            'status': 'ERR',
            'headings': None
            }

    return jsonify(result)

@app.route('/news/<string:source>')
def news(source):
    # return news headings of the particular 'source'
    conn = db.connect()

    # Perform query and return JSON data
    try:
        query = conn.execute(f"SELECT source, article_num, author, heading, top_image, summary, keywords, link, date_added FROM {source}")

        # Query the result and get cursor.Dumping that data to a JSON is looked by extension
        result = {
            'status': 'OK',
            'news-data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]
        }

    except Exception:
        result = {
            'status': 'ERR',
            'news-data': None
            }

    return jsonify(result)

if __name__ == '__main__':
    app.run(debug=True)