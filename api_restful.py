#! Python
"""
 @author    : Donald Chinhuru
 @created   : 12 April 2018
 @about     : This restful api in python and flask has been copied from
             url <https://impythonist.wordpress.com/2015/07/12/build-an-api-under-30-lines-of-code-with-python-and-flask/>
             > that i learned from and started to modify on top of, i give all credit to the author of the blog
 @aim       : create a restful api for news providers in Zim, a similar project to newsapi.org [ which by the time of the creation
              of my project, Zim news are not there ]
             > create an api that returns top headlines news, sports, politics, business, tech and
              anything related
             > might extend it not only to news but for other services like weather from my electronic
               projects etc
 @modified  : 20 Nov 2019

__ in development __
"""

from flask import Flask, jsonify
from flask_restful import Resource, Api
from sqlalchemy import create_engine

import config

# Create an engine for connecting to SQLite3.
# Assuming newsapi.db is in your app root folder

e = create_engine(f'sqlite:///{config.db_handler()}')

app = Flask(__name__)
api = Api(app)

class Ping(Resource):
    def get(self):
        # test mesg for restful api
        return jsonify("pong!")

class Index(Resource):
    def get(self):
        # print out a Index text to the user
        text = {
        'status': 'Flask Zim news restful api demo',
        'author': 'Donald C',
        'link': 'https://github.com/DonnC',
        'routes': {
            'index': 'http://127.0.0.1:5000/',
            'ping': 'http://127.0.0.1:5000/ping',
            'news-sources'  : 'http://127.0.0.1:5000/sources',
            'news-headings' : 'http://127.0.0.1:5000/headings/<source-name>',
            'news-data'     : 'http://127.0.0.1:5000/news/<source-name>'
            }
        }

        return jsonify(text)

class Heading(Resource):
    def get(self, source):
        # Connect to databse
        global e
        conn = e.connect()

        # Perform query and return JSON data
        try:
            query = conn.execute(f"SELECT DISTINCT heading FROM {source}")

            result = {
                'status': 'OK',
                'error-msg': None,
                'headings': [i[0] for i in query.cursor.fetchall()]
                }

        except Exception as e:
            result = {
                'status': 'ERR',
                'error-msg': e,
                'headings': None
                }

        return result

class News(Resource):
    def get(self, source):
        global e
        conn = e.connect()

        try:
            query = conn.execute(f"SELECT source, article_num, author, heading, top_image, summary, keywords, link, date_added FROM {source}")
            # Query the result and get cursor.Dumping that data to a JSON is looked by extension
            result = {
                'status': 'OK',
                'news-data': [dict(zip(tuple (query.keys()) ,i)) for i in query.cursor]
                }

        except Exception as e:
            result = {
                'status': 'ERR',
                'error-msg': e,
                'news-data': None
                }

        return result

class Sources(Resource):
    '''
        Get all news sources in the database
    '''
    def get(self):
        sources = config.db_tables()
        return {
            'info': 'use the source name to get info from other routes!',
            'news-sources': sources
            }

# create api objects and define out routing ( urls )
api.add_resource(Index, '/')
api.add_resource(Ping, '/ping')
api.add_resource(Heading, '/headings/<string:source>')
api.add_resource(News, '/news/<string:source>')
api.add_resource(Sources, '/sources')

if __name__ == '__main__':
    app.run(debug=True)
