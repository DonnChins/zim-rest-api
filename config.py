# config file for rest api
from os import path

ROOT_DIR = path.dirname(__file__)

db_name = "news_api.db"
db_folder = "database"
db_path = path.join(ROOT_DIR, db_folder)
db = path.join(db_path, db_name)

def db_handler():
    '''
        check if file exists
    '''
    if path.isfile(db):
        return db

    return None

def db_tables():
    # TODO need a way to query the database using sql to get all tables in the db
    # this is js a lazy method to hard code them
    sources = [
            'bulawayo24',
            'businessdaily',
            'chronicle',
            'dailynews',
            'financialgazette',
            'footbal_mole',
            'headlines_mole',
            'herald',
            'manicapost',
            'nehanda',
            'soccer24',
            'source',
            'standard',
            'sundaymail',
            'techzim',
            'trend_mole',
            'zim_supersport'
        ]

    return sources