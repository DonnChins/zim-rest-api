# zim-rest-api

flask restful api using zim news data

## Running rest api
- `api_pure_flask.py` is a rest api written with default flask package
- `api_restful.py` is a restful api written using the `Flask-RestFul` library
- first run any of the `api_x.py` files in a terminal
- in another terminal, run the `client.py` file or use `POSTMAN`

## More
- you can add more functions in `client.py`

### Flask RestFul api for zim news
- the database files have old news saved back in 2018
- to get recent news, current affairs consider using the `newspaper-zw` library i created 

``` sh
$ pip install newspaper-zw
```

## getting current affairs using the library
```python
from newspaperzw.news import News

# get current affairs from The Herald
source = 'herald'

try:
	api = News(provider=source, cache=False, summary=True)
	current_affairs = api.paper()
	
	# if all goes well, you can save the returned current_affairs to database or file
	print(current_affairs)
	
except Exception as e:
	print("[ERROR] There was an error: ", e)
	
```

- can save news to database to use for restful api

## available routing (on localhost)
- ping resful server
![ping](http://127.0.0.1:5000/ping)

- server index
![index](http://127.0.0.1:5000/)

- get all news from given source
![news](http://127.0.0.1:5000/news/<source-name>)

- headings or article topics from source
![headings](http://127.0.0.1:5000/headings/<source-name>)

- get all sources to user on <source-name>
![sources](http://127.0.0.1:5000/sources)